package com.omnichat.log.filter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.omnichat.common.dto.user.ImmutableOCUser;
import com.omnichat.common.enums.system.LogKey;
import com.omnichat.common.utils.OCStringUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toMap;

@Component
public class LogFilter extends OncePerRequestFilter {

  private static final Logger logger = LoggerFactory.getLogger(LogFilter.class);

  @Value("${spring.profiles.active}")
  private String activeProfile;

  @Value("${log.skip.endPoints:}")
  private Set<String> skipLoggingEndpoints;

  @Value("${log.reduce.endPoints:}")
  private Set<String> reduceLoggingPaths;

  // Default format is :method | :endpoint | :request | |
  @Value("${log.api.format:%s | %s | %s}")
  private String apiRequestLogFormat;

  // Default format is :method | :endpoint | :status_code | :response_time | :response
  @Value("${log.api.format:%s | %s | %s | %s | %s}")
  private String apiResponseLogFormat;

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    var traceId = UUID.randomUUID().toString();
    var attributes = RequestContextHolder.getRequestAttributes();
    if (attributes != null) {
      traceId = (String) attributes.getAttribute(LogKey.TRACE_ID, RequestAttributes.SCOPE_REQUEST);
      traceId = OCStringUtils.isNullOrEmpty(traceId) ? UUID.randomUUID().toString() : traceId;
      attributes.setAttribute(LogKey.TRACE_ID, traceId, RequestAttributes.SCOPE_REQUEST);
    }

    MDC.put(LogKey.TRACE_ID, traceId);

    logRequest(request);

    long startTime = System.currentTimeMillis();

    filterChain.doFilter(request, response);

    logResponse(request, response, startTime);

    MDC.clear();
  }

  private void logRequest(HttpServletRequest request) {
    String path = request.getServletPath();

    if (skipLoggingEndpoints.contains(path)) {
      return;
    }

    // Header fields to be logged, must be lowercase
    Map<String, String> headers =
        Stream.of("host", "user-agent", "x-line-signature", "x-hub-signature")
            .filter(name -> request.getHeader(name) != null)
            .collect(toMap(Function.identity(), request::getHeader));

    Map<String, Object> requestLog = new HashMap<>();
    requestLog.put("_header", headers);

    // Request Query Parameters
    Set<String> removeQueryParams = new HashSet<>();
    if (reduceLoggingPaths.contains(path)) {
      removeQueryParams.add("access-token");
    }

    Map<String, String> queryParameters =
        Arrays.stream(request.getQueryString().split("&"))
            .map(q -> q.split("="))
            .filter(s -> !removeQueryParams.contains(s[0]))
            .collect(toMap(s -> s[0], s -> s[1]));

    requestLog.put("_query", queryParameters);

    String team;

    // Team & User
    ImmutableOCUser ocUser =
        (ImmutableOCUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    if (ocUser != null) {
      team = ocUser.getTeam() == null ? "" : ocUser.getTeam();
      requestLog.put("_team", team);
      if (ocUser.getUsername() != null) {
        requestLog.put("_username", ocUser.getUsername());
      }
    }
    // Request Body
    String requestBody = null;
    try {
      requestBody = IOUtils.toString(request.getInputStream(), StandardCharsets.UTF_8.name());
    } catch (IOException e) {
      logger.error("Failed to extract the request body", e);
    }

    ObjectMapper objectMapper = new ObjectMapper();
    if (requestBody != null) {
      if (!activeProfile.equalsIgnoreCase("uat") && reduceLoggingPaths.contains(path)) {
        requestLog.put("_body", "[skipped]");
      } else {
        if (requestBody.startsWith("{") || requestBody.startsWith("[")) {
          try {
            requestLog.put("_body", objectMapper.readValue(requestBody, Map.class));
          } catch (JsonProcessingException e) {
            logger.error("Failed to deserialize the request body", e);
          }
        } else {
          requestLog.put("_body", "[nologging]");
        }
      }
    }

    try {
      logger.info(
          String.format(
              apiRequestLogFormat,
              request.getMethod(),
              path,
              objectMapper.writeValueAsString(requestLog)));
    } catch (JsonProcessingException e) {
      logger.error("Failed to log the response body", e);
    }
  }

  private void logResponse(
      HttpServletRequest request, HttpServletResponse response, long startTime) {
    String path = request.getServletPath();
    if (skipLoggingEndpoints.contains(path)) {
      return;
    }

    long responseTime = System.currentTimeMillis() - startTime;

    // Extract response
    Map<String, Object> responseLog = new HashMap<>();
    ContentCachingResponseWrapper responseEntity = new ContentCachingResponseWrapper(response);
    String responseBody =
        IOUtils.toString(responseEntity.getContentAsByteArray(), StandardCharsets.UTF_8.name());

    boolean shouldSkipLogginResponse =
        !activeProfile.equalsIgnoreCase("uat") && reduceLoggingPaths.contains(path);

    ObjectMapper objectMapper = new ObjectMapper();

    // skip logging response only if response is not error
    if (shouldSkipLogginResponse) {
      responseLog.put("_body", "[skipped]");
    } else {
      if (responseBody.startsWith("[") || responseBody.startsWith("{")) {
        try {
          responseLog.put("_body", objectMapper.readValue(responseBody, Map.class));
        } catch (JsonProcessingException e) {
          logger.error("Failed to deserialize the response body", e);
        }
      } else {
        responseLog.put("_body", responseBody);
      }
    }

    try {
      logger.info(
          String.format(
              apiResponseLogFormat,
              request.getMethod(),
              path,
              response.getStatus(),
              responseTime,
              objectMapper.writeValueAsString(responseLog)));
    } catch (JsonProcessingException e) {
      logger.error("Failed to log the response body", e);
    }
  }
}
