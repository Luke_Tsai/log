package com.omnichat.log;

import org.springframework.boot.web.servlet.filter.OrderedRequestContextFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.RequestContextFilter;

@ComponentScan
@Configuration
public class LogConfig {

  @Bean
  public RequestContextFilter requestContextFilter() {
    return new OrderedRequestContextFilter();
  }
}
